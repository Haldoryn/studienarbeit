from textx import metamodel_from_file
from textx.model import get_children_of_type
import textx.scoping as scoping
from textx.exceptions import TextXSyntaxError

metamodel = metamodel_from_file('Schlangennest/Gramma/Kennzeichen.tx',debug=False)
try:
    textmodel = metamodel.model_from_str('=BFA01 QA002')##32ULE341916.WBW02 ==MDK13 GP001 .AA01 &AAA001/01
except TextXSyntaxError as err:
    print(err.col)
    print(err.expected_rules)
    print(err.message)
    quit()



gemainsameZuordnung = get_children_of_type("GemainsameZuordnung", textmodel)
print(len(gemainsameZuordnung))

funktionaleZuordnung = get_children_of_type("FunktionsbezogeneKennzeichnung", textmodel)
print(len(funktionaleZuordnung))



dokumentenKennzeichung = get_children_of_type("DokumentenKennzeichung", textmodel)
print(len(dokumentenKennzeichung))

spezifischesKennzeichen = get_children_of_type("SpezifischesKennzeichen", textmodel)
print(len(spezifischesKennzeichen))

print(textmodel.referenzKennzeichen.funktion)
print(metamodel.register_scope_providers({"*.*": scoping.providers.FQN()})) 