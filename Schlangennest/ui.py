import tkinter
from tkinter import Button
from textx import metamodel_from_file
from textx.model import get_children_of_type
import textx.scoping as scoping
from textx.exceptions import TextXSyntaxError

metamodel = metamodel_from_file('Gramma/Kennzeichen.tx',debug=False)

window = tkinter.Tk()
window.title("RDS-PP Editor")
label = tkinter.Label(window, text = "OK")
label.grid(column=0, row=1)

def parsExeption(exeption):
    exlist = exeption.split()
    listofallposibilitis = []
    [listofallposibilitis.append(item) for item in exlist[:-1] if item not in listofallposibilitis and item[0]=='\'']
    return ' '.join([exlist[0]] + listofallposibilitis + [exlist[-6], exlist[-5], exlist[-4], exlist[-3], exlist[-2], exlist[-1]])
    
def setSystem(system):
    if hasattr(system, 'hauptsystem'): hauptsystem.set(system.hauptsystem)
    if hasattr(system, 'systemschlüssel'): systemschlüssel.set(system.systemschlüssel)
    if hasattr(system, 'zähler'): zählerS.set(system.zähler)

def setGrundfunktion(grundfunktion):
    if hasattr(grundfunktion, 'grundfunktion'): grundfunktionvar.set(grundfunktion.grundfunktion)
    if hasattr(grundfunktion, 'zählung'): zählergf.set(grundfunktion.zählung)

def setProdukt(produkt):
    produktstring = ""
    for prod in produkt.gliederungsstufe:
        produktstring += prod.produktklasse
        produktstring += prod.zählteil
    produktklassen.set(produktstring)

def setFunktionskenn(funktionskenn):
    setSystem(funktionskenn.system)
    setGrundfunktion(funktionskenn.grundfunktion)

def setFunktionsZuordnung(funkzu):
    if hasattr(funkzu, 'system'): 
        setSystem(funkzu.system)
        radioVarFB.set("SYS")
    if hasattr(funkzu, 'grundfunktion'): setGrundfunktion(funkzu.grundfunktion)
    if hasattr(funkzu, 'funktionsbereich'): 
        funktionsbereich.set(funkzu.funktionsbereich)
        radioVarFB.set("FB")
    if hasattr(funkzu, 'funktionsgruppe'): funktionsgruppe.set(funkzu.funktionsgruppe)
    if hasattr(funkzu, 'leitfunktion'): leitfunktion.set(funkzu.leitfunktion)

def setBetriebsmittel(betriebsmittel):
    if hasattr(betriebsmittel, 'funktion'): setFunktionskenn(betriebsmittel.funktion)
    if hasattr(betriebsmittel, 'produkt'): setProdukt(betriebsmittel.produkt)

def setEinabuort(einbauort):
    if hasattr(einbauort, 'betriebsmittel'): setBetriebsmittel(einbauort.betriebsmittel)
    einbausysstring = ''
    if hasattr(einbauort, 'system'): einbausysstring += einbauort.system.hauptsystem + einbauort.system.systemschlüssel + einbauort.system.zähler
    if hasattr(einbauort, 'technischeEinrichtung'): einbausysstring += einbauort.technischeEinrichtung.grundfunktion + einbauort.technischeEinrichtung.zählung
    einbausystem.set(einbausysstring)
    if hasattr(einbauort, 'einbauplatz'): einbauplatz.set(einbauort.einbauplatz)

def setAufstellort(aufstellort):
    if hasattr(aufstellort, 'betriebsmittel'): setBetriebsmittel(aufstellort.betriebsmittel)
    if hasattr(aufstellort, 'system'): aufstellsystem.set(aufstellort.system.hauptsystem + aufstellort.system.systemschlüssel + aufstellort.system.zähler)
    if hasattr(aufstellort, 'raum'): raum.set(aufstellort.raum)

def setDokument(dokument):
    dukumentenartklassenschlüssel.set(dokument.dukumentenartklassenschlüssel)
    dokzähler.set(dokument.zähler)
    if hasattr(dokument, 'seitenzahl'): seitenzahl.set(dokument.seitenzahl)

def setAnschluss(anschl):
    anschlusselemente.set(anschl.anschlusselemente)
    anschlüsse.set(anschl.anschlüsse)

def setGZ(sig):
    if hasattr(sig.standortPosition, 'zonenfeld'): 
        zonenfeld.set(sig.standortPosition.zonenfeld)
        region.set(sig.standortPosition.region)        
        xKoordinate.set(sig.standortPosition.xKoordinate)
        yKoordinate.set(sig.standortPosition.yKoordinate)
        utmstandortbezeichnung.set(sig.standortbezeichnung)
        bzRB.set("UTMREF")

    if hasattr(sig.standortPosition, 'horizontaleUnterteilung'): 
        horizontaleUnterteilung.set(sig.standortPosition.horizontaleUnterteilung + sig.standortPosition.ns)
        vertikaleUnterteilung.set(sig.standortPosition.vertikaleUnterteilung + sig.standortPosition.ew)
        wgsstandortbezeichnung.set(sig.standortbezeichnung)
        bzRB.set("WGS84")


def setSignal(sig):
    signalklassen.set(sig.signalklassen)
    weitereSignalklassen.set(sig.weitereSignalklassen)
    if hasattr(sig, 'basisname'): basisname.set(sig.basisname)

def onChange(*arg):
    try:
        textmodel = metamodel.model_from_str(kennzeichnung.get())
        for ref in get_children_of_type("AufstellungsortKennzeichnung", textmodel): 
            setAufstellort(ref) 
            refRB.set("Aufstellungsort Kennzeichnung")
        for ref in get_children_of_type("EinbauortKennzeichnung", textmodel): 
            setEinabuort(ref) 
            refRB.set("Einbauort Kennzeichnung")
        for ref in get_children_of_type("BetriebsmittelKennzeichnung", textmodel): 
            setBetriebsmittel(ref) 
            refRB.set("Betriebsmittel Kennzeichnung")
        for ref in get_children_of_type("FunktionaleZuordnung", textmodel): 
            setFunktionsZuordnung(ref) 
            refRB.set("Funktionale Zuordnung")
        for ref in get_children_of_type("FunktionsbezogeneKennzeichnung", textmodel): 
            setFunktionskenn(ref)
            refRB.set("Funktionsbezogene Kennzeichnung")
        for ref in get_children_of_type("ProduktKennzeichnung", textmodel): 
            setProdukt(ref)
            refRB.set( "Produkt Kennzeichnung")
        for ref in get_children_of_type("SignalKennzeichnung", textmodel): 
            setSignal(ref)
            sprzifischRB.set( "Signal")
        for ref in get_children_of_type("DokumentenKennzeichung", textmodel): 
            setDokument(ref)
            sprzifischRB.set( "Dokument")
        for ref in get_children_of_type("AnschlussKennzeichnung", textmodel): 
            setAnschluss(ref)
            sprzifischRB.set( "Anschluss")
        for ref in get_children_of_type("GemainsameZuordnung", textmodel): 
            setGZ(ref)

    except TextXSyntaxError as err:
        ex = parsExeption(err.message)
        label.configure(text=ex)
    else:
        label.configure(text="OK")

    

kennzeichnung = tkinter.StringVar() 
kennzeichnung.trace("w", onChange)

text = tkinter.Entry(window, width=100, textvariable=kennzeichnung)
text.grid(column=0, row=0)

kennzeichnungFrame = tkinter.Frame(window)
kennzeichnungFrame.grid(column = 0, row = 2)

gzFrame = tkinter.LabelFrame(kennzeichnungFrame, text="Gemeinsame Zuordnung")
gzFrame.grid(column=0, row=2)

referenzFrame = tkinter.LabelFrame(kennzeichnungFrame, text="Referenz Kennzeichnung")
referenzFrame.grid(column=1, row=2)

spezifischeFrame = tkinter.LabelFrame(kennzeichnungFrame, text="Spezifische Kennzeichnung")
spezifischeFrame.grid(column=3, row=2)

def kennzeichenEingabe(*arg):
    teststring = GemeinsameZuordnungString() + referenzKennzeichenString() + spezKennzeichenString()
    kennzeichnung.set(teststring)

    
def refchange(*arg):
    systemColor("white")
    grundfunktionColor("white")
    betriebsmittelColor("white")
    funktionaleZuordnungColor("white")
    funktionsbezogeneColor("white")
    produktColor("white")
    aufstellortColor("white")
    einbauColor("white")
    rbstring = refRB.get()
    if rbstring == "Betriebsmittel Kennzeichnung": return betriebsmittelColor("green")
    elif rbstring == "Funktionale Zuordnung": return funktionaleZuordnungColor("green")
    elif rbstring == "Funktionsbezogene Kennzeichnung": return funktionsbezogeneColor("green")
    elif rbstring == "Produkt Kennzeichnung": return produktColor("green")
    elif rbstring == "Aufstellungsort Kennzeichnung": return aufstellortColor("green")
    elif rbstring == "Einbauort Kennzeichnung": return einbauColor("green")
    print(refRB.get())
refRB = tkinter.StringVar() 
refRB.trace("w", refchange)
refRB.set("None")

tkinter.Button(window, text="Übernehmen", command=kennzeichenEingabe).grid(column=0, row=3, columnspan=3)

def GemeinsameZuordnungString():
    rbstring = bzRB.get()
    if rbstring == "None": return ""
    elif rbstring == "UTMREF": return UTMREFString()
    elif rbstring == "WGS84": return WGS84String()
    print("RIP")

def bzChange(*arg):
    utmColor("white")
    wgsColor("white")
    rbstring = bzRB.get()
    if rbstring == "UTMREF": return utmColor("green")
    elif rbstring == "WGS84": return wgsColor("green")
bzRB = tkinter.StringVar()
bzRB.set("None")
bzRB.trace("w", bzChange)
tkinter.Radiobutton(gzFrame, variable = bzRB, value = "None").grid(column=0, row=0)
tkinter.Label(gzFrame, text="None").grid(column=1, row=0)

def UTMREFString():
    sigstr = "#" + zonenfeld.get() + region.get() + xKoordinate.get() + yKoordinate.get() + "." + utmstandortbezeichnung.get()
    return sigstr
def utmColor(color):
    for entry in utmList:
        entry.config(bg=color)

zonenfeld = tkinter.StringVar()
region = tkinter.StringVar()
xKoordinate = tkinter.StringVar()
yKoordinate = tkinter.StringVar()
utmstandortbezeichnung = tkinter.StringVar()

tkinter.Radiobutton(gzFrame, variable = bzRB, value = "UTMREF").grid(column=0, row=1)

utmframe = tkinter.LabelFrame(gzFrame, text="UTMREF")
utmframe.grid(column=1, row=1)
tkinter.Label(utmframe, text="Zonenfeld").grid(column=0, row=0)
tkinter.Label(utmframe, text="Region").grid(column=0, row=1)
tkinter.Label(utmframe, text="xKoordinate").grid(column=0, row=2)
tkinter.Label(utmframe, text="yKoordinate").grid(column=0, row=3)
tkinter.Label(utmframe, text="Standortbezeichnung").grid(column=0, row=4)

utmList = [
    tkinter.Entry(utmframe, textvariable = zonenfeld),
    tkinter.Entry(utmframe, textvariable = region),
    tkinter.Entry(utmframe, textvariable = xKoordinate),
    tkinter.Entry(utmframe, textvariable = yKoordinate),
    tkinter.Entry(utmframe, textvariable = utmstandortbezeichnung)
]
for i in range(5):
    utmList[i].grid(column=1, row=i)

def WGS84String():
    sigstr = "#" + horizontaleUnterteilung.get() + vertikaleUnterteilung.get() + "." + wgsstandortbezeichnung.get()
    return sigstr
def wgsColor(color):
    for entry in wgsList:
        entry.config(bg=color)

horizontaleUnterteilung = tkinter.StringVar()
vertikaleUnterteilung = tkinter.StringVar()
wgsstandortbezeichnung = tkinter.StringVar()

tkinter.Radiobutton(gzFrame, variable = bzRB, value = "WGS84").grid(column=0, row=2)

WGS84frame = tkinter.LabelFrame(gzFrame, text="WGS84")
WGS84frame.grid(column=1, row=2)
tkinter.Label(WGS84frame, text="Horizontale Unterteilung").grid(column=0, row=0)
tkinter.Label(WGS84frame, text="Vertikale Unterteilung").grid(column=0, row=1)
tkinter.Label(WGS84frame, text="standortbezeichnung").grid(column=0, row=2)

wgsList = [
    tkinter.Entry(WGS84frame, textvariable = horizontaleUnterteilung),
    tkinter.Entry(WGS84frame, textvariable = vertikaleUnterteilung),
    tkinter.Entry(WGS84frame, textvariable = wgsstandortbezeichnung)
]
for i in range(3):
    wgsList[i].grid(column=1, row=i)

def spezKennzeichenString():
    rbstring = sprzifischRB.get()
    if rbstring == "None": return ""
    elif rbstring == "Signal": return signalString()
    elif rbstring == "Anschluss": return anschlussString()
    elif rbstring == "Dokument": return docString()
    print("RIP")

def spezChange(*arg):
    signalColor("white")
    anschlussColor("white")
    docColor("white")
    rbstring = sprzifischRB.get()
    if rbstring == "Signal": return signalColor("green")
    elif rbstring == "Anschluss": return anschlussColor("green")
    elif rbstring == "Dokument": return docColor("green")

sprzifischRB = tkinter.StringVar()
sprzifischRB.set("None")
sprzifischRB.trace("w", spezChange)
tkinter.Radiobutton(spezifischeFrame, variable = sprzifischRB, value = "None").grid(column=0, row=0)
tkinter.Label(spezifischeFrame, text="None").grid(column=1, row=0)

def signalString():
    sigstr = ";" + signalklassen.get() + weitereSignalklassen.get()
    if basisname.get() != "": sigstr += "." + basisname.get()
    return sigstr
def signalColor(color):
    for entry in sigList:
        entry.config(bg=color)
signalklassen = tkinter.StringVar()
weitereSignalklassen = tkinter.StringVar()
basisname = tkinter.StringVar()
tkinter.Radiobutton(spezifischeFrame, variable = sprzifischRB, value = "Signal").grid(column=0, row=1)
signalframe = tkinter.LabelFrame(spezifischeFrame, text="Signal Kennzeichnung")
signalframe.grid(column=1, row=1)
tkinter.Label(signalframe, text="Signal Klasse").grid(column=0, row=0)
tkinter.Label(signalframe, text="Weitere Signal Klasse").grid(column=0, row=1)
tkinter.Label(signalframe, text="Basisname").grid(column=0, row=2)
sigList = [
    tkinter.Entry(signalframe, textvariable = signalklassen),
    tkinter.Entry(signalframe, textvariable = weitereSignalklassen),
    tkinter.Entry(signalframe, textvariable = basisname)
]
for i in range(3):
    sigList[i].grid(column=1, row=i)

def anschlussString():
    return ":" + anschlusselemente.get() + "." + anschlüsse.get()
def anschlussColor(color):
    for entry in anschlussList:
        entry.config(bg=color)
anschlusselemente = tkinter.StringVar()
anschlüsse = tkinter.StringVar()
tkinter.Radiobutton(spezifischeFrame, variable = sprzifischRB, value = "Anschluss").grid(column=0, row=2)
anschlussframe = tkinter.LabelFrame(spezifischeFrame, text="Anschluss Kennzeichnung")
anschlussframe.grid(column=1, row=2)
tkinter.Label(anschlussframe, text="Anschluss Elemente").grid(column=0, row=0)
tkinter.Label(anschlussframe, text="Anschlüsse").grid(column=0, row=1)
anschlussList = [
    tkinter.Entry(anschlussframe, textvariable = anschlusselemente),
    tkinter.Entry(anschlussframe, textvariable = anschlüsse)
]
for i in range(2):
    anschlussList[i].grid(column=1, row=i)


def docString():
    sigstr = "&" + dukumentenartklassenschlüssel.get() + dokzähler.get()
    if seitenzahl.get() != "": sigstr += "/" + seitenzahl.get()
    return sigstr
def docColor(color):
    for entry in docList:
        entry.config(bg=color)
dukumentenartklassenschlüssel = tkinter.StringVar()
dokzähler = tkinter.StringVar()
seitenzahl = tkinter.StringVar()
tkinter.Radiobutton(spezifischeFrame, variable = sprzifischRB, value = "Dokument").grid(column=0, row=3)
dokframe = tkinter.LabelFrame(spezifischeFrame, text="Dokumenten Kennzeichnung")
dokframe.grid(column=1, row=3)
tkinter.Label(dokframe, text="Dukumentenartklassenschlüssel").grid(column=0, row=0)
tkinter.Label(dokframe, text="Zähler").grid(column=0, row=1)
tkinter.Label(dokframe, text="Seitenzahl").grid(column=0, row=2)
docList = [
    tkinter.Entry(dokframe, textvariable = dukumentenartklassenschlüssel),
    tkinter.Entry(dokframe, textvariable = dokzähler),
    tkinter.Entry(dokframe, textvariable = seitenzahl)
]
for i in range(3):
    docList[i].grid(column=1, row=i)

def referenzKennzeichenString():
    rbstring = refRB.get()
    if rbstring == "None": return ""
    elif rbstring == "Betriebsmittel Kennzeichnung": return betriebsmittelString()
    elif rbstring == "Funktionale Zuordnung": return funktionaleZuordnungString()
    elif rbstring == "Funktionsbezogene Kennzeichnung": return funktionsbezogeneString()
    elif rbstring == "Produkt Kennzeichnung": return produktString()
    elif rbstring == "Aufstellungsort Kennzeichnung": return aufstellortString()
    elif rbstring == "Einbauort Kennzeichnung": return einbauortString()

def systemString():
    return hauptsystem.get() + systemschlüssel.get() + zählerS.get()
def systemColor(color):
    for entry in systemList:
        entry.config(bg=color)
systemFrame = tkinter.LabelFrame(referenzFrame, text="System")
systemFrame.grid(column=3, row=0)
hauptsystem = tkinter.StringVar()
systemschlüssel = tkinter.StringVar()
zählerS = tkinter.StringVar()
tkinter.Label(systemFrame, text = "Hauptsystem").grid(column=0, row=0) 
tkinter.Label(systemFrame, text = "System Schlüssel").grid(column=0, row=1) 
tkinter.Label(systemFrame, text = "Zähler").grid(column=0, row=2) 

systemList = [
    tkinter.Entry(systemFrame, textvariable=hauptsystem),
    tkinter.Entry(systemFrame, textvariable=systemschlüssel),
    tkinter.Entry(systemFrame, textvariable=zählerS)
]
for i in range(3):
    systemList[i].grid(column=1, row=i)


def grundfunktionString():
    return grundfunktionvar.get() + zählergf.get()
def grundfunktionColor(color):
    for entry in grundfunktionList:
        entry.config(bg=color)
gfFrame = tkinter.LabelFrame(referenzFrame, text="Grundfunktion")
gfFrame.grid(column=3, row=1)
grundfunktionvar = tkinter.StringVar()
zählergf = tkinter.StringVar()
tkinter.Label(gfFrame, text = "Grundfunktion").grid(column=0, row=0) 
tkinter.Label(gfFrame, text = "Zähler").grid(column=0, row=1) 
grundfunktionList = [
    tkinter.Entry(gfFrame, textvariable=grundfunktionvar),
    tkinter.Entry(gfFrame, textvariable=zählergf)
]
for i in range(2):
    grundfunktionList[i].grid(column=1, row=i)


noRB = tkinter.Radiobutton(referenzFrame, variable=refRB, value="None")
noRB.grid(column=0, row=0)
nolabel = tkinter.Label(referenzFrame, text = "None")
nolabel.grid(column=1, row=0)

def betriebsmittelString():
    return funktionsbezogeneString() + produktString()
def betriebsmittelColor(color):
    funktionsbezogeneColor(color)
    produktColor(color)
betriebsmittelRB = tkinter.Radiobutton(referenzFrame, variable=refRB, value="Betriebsmittel Kennzeichnung")
betriebsmittelRB.grid(column=0, row=1)
betriebsmittelFrame = tkinter.Label(referenzFrame, text="Betriebsmittel Kennzeichnung")
betriebsmittelFrame.grid(column=1, row=1)

def funktionaleZuordnungString():
    returnstring = "=="
    rbstring = radioVarFB.get()
    if rbstring == "FB":
        returnstring += funktionsbereich.get() + funktionsgruppe.get()
    elif rbstring == "SYS":
        returnstring += systemString() + grundfunktionString()
    return returnstring + "." + leitfunktion.get()

def funktionaleZuordnungColor(color):
    rbstring = radioVarFB.get()
    leitfunktionEntry.config(bg=color)
    if rbstring == "FB":
        for entry in funktionsbereichList:
            entry.config(bg=color)
    elif rbstring == "SYS":
        for entry in funktionsbereichList:
            entry.config(bg="white")
        systemColor(color)
        grundfunktionColor(color)

funktionaleZuordnungRB = tkinter.Radiobutton(referenzFrame, variable=refRB, value="Funktionale Zuordnung")
funktionaleZuordnungRB.grid(column=0, row=2)
funktionaleZuordnungFrame = tkinter.LabelFrame(referenzFrame, text="Funktionale Zuordnung")
funktionaleZuordnungFrame.grid(column=1, row=2)
radioVarFB = tkinter.StringVar()
radioVarFB.set("FB")
radioFB = tkinter.Radiobutton(funktionaleZuordnungFrame, variable=radioVarFB, value="FB")
radioVarFB.trace("w", refchange)
radioFB.grid(column=0, row=0)
tkinter.Label(funktionaleZuordnungFrame, text="System und Grundfunktion").grid(column=1, row=1)
radioSYS = tkinter.Radiobutton(funktionaleZuordnungFrame, variable=radioVarFB, value="SYS")
radioSYS.grid(column=0, row=1)
fBFrame = tkinter.Frame(funktionaleZuordnungFrame) 
fBFrame.grid(column=1, row=0)
funktionsbereich = tkinter.StringVar()
funktionsgruppe = tkinter.StringVar()
tkinter.Label(fBFrame, text = "Funktionsbereich").grid(column=0, row=0) 
tkinter.Label(fBFrame, text = "Funktionsgruppe").grid(column=0, row=1) 

funktionsbereichList = [
    tkinter.Entry(fBFrame, textvariable=funktionsbereich),
    tkinter.Entry(fBFrame, textvariable=funktionsgruppe),
]
for i in range(2):
    funktionsbereichList[i].grid(column=0, row=i) 

leitfunktion = tkinter.StringVar()
tkinter.Label(funktionaleZuordnungFrame, text = "Funktionsgruppe").grid(column=0, row=2) 
leitfunktionEntry = tkinter.Entry(funktionaleZuordnungFrame, textvariable=leitfunktion)
leitfunktionEntry.grid(column=1, row=2)

def funktionsbezogeneString():
    fbString = "=" + systemString() + grundfunktionString()
    print("FBSTRING", fbString)
    return fbString
def funktionsbezogeneColor(color):
    systemColor(color)
    grundfunktionColor(color)

funktionsbezogeneKennzeichnungRB = tkinter.Radiobutton(referenzFrame, variable=refRB, value="Funktionsbezogene Kennzeichnung")
funktionsbezogeneKennzeichnungRB.grid(column=0, row=3)
funktionsbezogeneKennzeichnungLabel = tkinter.Label(referenzFrame, text="Funktionsbezogene Kennzeichnung")
funktionsbezogeneKennzeichnungLabel.grid(column=1, row=3)

def produktString():
    return "-" + produktklassen.get()
def produktColor(color):
    produktEntry.config(bg=color)
produktKennzeichnungRB = tkinter.Radiobutton(referenzFrame, variable=refRB, value="Produkt Kennzeichnung")
produktKennzeichnungRB.grid(column=0, row=4)
produktKennzeichnungFrame = tkinter.LabelFrame(referenzFrame, text="Produkt Kennzeichnung")
produktKennzeichnungFrame.grid(column=1, row=4)
produktklassen = tkinter.StringVar()
tkinter.Label(produktKennzeichnungFrame, text = "Produkt Klassen").grid(column=0, row=0) 
produktEntry = tkinter.Entry(produktKennzeichnungFrame, textvariable=produktklassen)
produktEntry.grid(column=1, row=0)

def aufstellortString():
    return betriebsmittelString() + "++" + aufstellsystem.get() + "." + raum.get()
def aufstellortColor(color):
    betriebsmittelColor(color)
    for entry in aufstellortList:
        entry.config(bg=color)
aufstellungsortKennzeichnungRB = tkinter.Radiobutton(referenzFrame, variable=refRB, value="Aufstellungsort Kennzeichnung")
aufstellungsortKennzeichnungRB.grid(column=0, row=5)
aufstellungsortKennzeichnungFrame = tkinter.LabelFrame(referenzFrame, text="Aufstellungsort Kennzeichnung")
aufstellungsortKennzeichnungFrame.grid(column=1, row=5)
raum = tkinter.StringVar()
aufstellsystem = tkinter.StringVar()
tkinter.Label(aufstellungsortKennzeichnungFrame, text = "Aufstellsystem").grid(column=0, row=0) 
tkinter.Label(aufstellungsortKennzeichnungFrame, text = "Raum").grid(column=0, row=1) 

aufstellortList = [
    tkinter.Entry(aufstellungsortKennzeichnungFrame, textvariable=aufstellsystem),
    tkinter.Entry(aufstellungsortKennzeichnungFrame, textvariable=raum)
]
for i in range(2):
    aufstellortList[i].grid(column=1, row=i)


def einbauortString():
    return betriebsmittelString() + "+" + systemString() + grundfunktionString() + "." + einbauplatz.get()

def einbauColor(color):
    for entry in einbauortList:
        entry.config(bg=color)

einbauortKennzeichnungRB = tkinter.Radiobutton(referenzFrame, variable=refRB, value="Einbauort Kennzeichnung")
einbauortKennzeichnungRB.grid(column=0, row=6)
einbauortKennzeichnungFrame = tkinter.LabelFrame(referenzFrame, text="Einbauort Kennzeichnung")
einbauortKennzeichnungFrame.grid(column=1, row=6)
einbausystem = tkinter.StringVar()
tkinter.Label(einbauortKennzeichnungFrame, text = "Einbausystem").grid(column=0, row=0) 
einbauplatz = tkinter.StringVar()
tkinter.Label(einbauortKennzeichnungFrame, text = "Einbauplatz").grid(column=0, row=1) 

einbauortList = [
    tkinter.Entry(einbauortKennzeichnungFrame, textvariable=einbausystem),
    tkinter.Entry(einbauortKennzeichnungFrame, textvariable=einbauplatz)
]
for i in range(2):
    einbauortList[i].grid(column=1, row=i)

window.mainloop()

